'use strict'
var jwt = require('jwt-simple');
var moment = require('moment');
var secret = "clave-secreta-de-token4445544"
exports.authenticate = 	function(req, res, next){
	//comprobar si llega el token(en el header de authorization)
	if(!req.headers.authorization){
		return res.status(403).send({
			message:'el header authorization esta vacio'
		});
	}
	// limpiar y capturar el token desde la cabecera de authorization
	var token = req.headers.authorization.replace(/['"]+/g, '');

	//decodificar el token

	try {
		var payload = jwt.decode(token, secret);
//comprobar q el token no halla expirado
			if(payload.exp <=moment().unix()){
				return res.status(403).send({
			message:'el token ha expirado'
		});
			}
	}catch(ex){
		return res.status(403).send({
			message:'el token no es valido'
		});
		//grabar usuario
		req.user = payload;
	}

	console.log("estas pasando por el middleware");
	
	next();
};