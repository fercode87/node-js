'use strict'

//requires
var express = require('express');
var bodyParser = require('body-parser');

//ejecutar express
var app = express();

//cargar archivos rutas
var user_routes = require('./routes/user');
var topic_routes = require('./routes/topic');
//Middlewares
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
//CORS

//Reescribir rutas
app.use('/api', user_routes);
app.use('/api', topic_routes);
//rutas de prueba

//Exportar modulo
module.exports = app;