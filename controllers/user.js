'use strict'

var validator = require ('validator');
var User = require ('../models/User');
var bcrypt = require ('bcrypt-nodejs');
var jwt = require('../services/jwt');
var fs = require ('fs');

var controller={
	probando: function(req, res){
		return res.status(200).send({
			message:"Soy la funcion probando"
		});
	},
	testeando: function(req, res){
		return res.status(200).send({
			message:"Soy la funcion testeando"
		});
	},
	save: function(req, res){
		//recoger parametros
		var params = req.body;
		//validar los datos
		try{
		var validate_name = !validator.isEmpty(params.name);
		var validate_surname = !validator.isEmpty(params.surname);
		var validate_email = !validator.isEmpty(params.email) && validator.isEmail(params.email);
		var validate_password = !validator.isEmpty(params.password);
		}catch{
		return res.status(404).send({
					message:"Faltan datos"
					});
	}

		//console.log(validate_name, validate_password, validate_email, validate_surname);
if(validate_name && validate_surname && validate_email && validate_password){
		//crear objeto usuario
		var user = new User;
		//asignar valores
		user.name = params.name;
		user.surname = params.surname;
		user.email = params.email;
		user.role = 'ROLE_USER';
		user.image = null;
     	//comprobar si existe el usuario
     	User.findOne({email: user.email}, (err, issetUser)=>{
			     		if(err){
			     			return res.status(500).send({
						message:"error en la validacion de duplicidad"
						});
			     	}
				     	if(!issetUser){
						//cifrar la contraseña
				     		
				     		bcrypt.hash(params.password, null, null,(err, hash)=>{
				     		user.password = hash;
						// guardar usuario
				     		
				     		user.save((err, userStored)=>{
				     			if(err){
			     			return res.status(500).send({
								message:"error al intentar guardar el usuario"
									});
			     					}
			     					if(!userStored){
							     			return res.status(500).send({
										message:"El usuario no se ha guardado"
										});
							     	}
							//enviar respuesta
									return res.status(200).send({
									status:'success',
									user: userStored
												});

				     		});//cierre de save	
				     					

				     		})
				     		

				     	}else{
				     		return res.status(500).send({
							message:"el usuario ya esta registrado"
							});
				     	}
				     	});
     	

	
}else{
	return res.status(200).send({
			message:"la validacion es incorrecta"
		})
		};
	
	},
	login: function(req, res){
		//Recoger parametros de la peticion
		var params = req.body; 
		//validar los datos
		try{
		var validate_email = !validator.isEmpty(params.email) && validator.isEmail(params.email);
		var validate_password = !validator.isEmpty(params.password);
		}catch{
		return res.status(404).send({
					message:"Faltan datos"
					});
	}
		if(!validate_email || !validate_password){
			return res.status(500).send({
							message:"los datos ingresados no son validos, vuelve a intentarlo"
							});
		}
		//buscar usuarios q concuerden con el email
		User.findOne({email: params.email.toLowerCase()},(err, user)=>{
			if(err){
			     			return res.status(500).send({
								message:"error al intentar loguearse"
									});
			     		}
			 if(!user){
			 	return res.status(500).send({
								message:"el usuario no existe"
									});
			 }
			 //Comprobar si son iguales el usuario con la contrasña de ese usuario
			 bcrypt.compare(params.password, user.password, (err, check)=>{
			 	if(check){

			 		if(params.gettoken){
			 			return res.status(200).send({
						token: jwt.createToken(user)
				});
			 	
			 		}
				}else{
			 		return res.status(404).send({
					message:"el usuario o contraseña no son validos"
					});

			 	};
			 });

		});
	},
	update: function(req, res){
		//recoger datos
		var params = req.body;
		//validar los datos
		try{
		var validate_name = !validator.isEmpty(params.name);
		var validate_surname = !validator.isEmpty(params.surname);
		var validate_email = !validator.isEmpty(params.email) && validator.isEmail(params.email);
	}catch{
		return res.status(404).send({
					message:"Faltan datos"
					});
	}
	//eliminar parametros que no quiero actualizar
	delete params.password;
	//Crear variable para utilizar el id de usuario
 	var userId = req.body.sub;
 	//buscar y actualizar Usuario
	User.findOne({email: params.email.toLowerCase()},(err, user)=>{
			if(err){
			     			return res.status(500).send({
								message:"Error al intentar modificar el usuario"
									});
			     		}
			 if(user && user.email==params.email){
			 	return res.status(500).send({
								message:"El email no puede ser modificado"
									});
			 }

 	else{
 	User.findOneAndUpdate({sub: userId}, params, {new: true}, (err, UserUpdated)=>{
 		if(err){
 			return res.status(404).send({
					status:'error',
					message:"El usuario no se actualizo por capturar el ERR"
					});
 		}
 		if(!UserUpdated){
 			return res.status(404).send({
					status:'error',
					message:"El usuario no se actualizo por falta de UserUpdated"
					});
 		}
 		return res.status(200).send({
 			status:'success',
 			user: UserUpdated
 		});
 	});

	}
	});
},	
 uploadAvatar: function(req, res){
 	//configurar el modulo multiparty(middleware) hecho en routes.js
	//conseguir el nombre y extension del archivo subido
	var file_name = 'Archivo no subido...';

	if(!req.files){
		return res.status(404).send({
 		message: "No se recibio ningun archivo"
 	});
	}
	var file_path =req.files.file0.path;
	var file_split = file_path.split('\\');
	var file_name = file_split[2];

	//comprobar q sea una extension de imagen
 	var ext_split = file_name.split('\.');
 	var file_ext = ext_split[1];

 	if(file_ext != 'png' && file_ext != 'jpg' && file_ext != 'jpeg' && file_ext != 'gif'){
		fs.unlink(file_path, (err)=>{
		return res.status(404).send({
 		status: 'error',
 		message: "Extension de archivo no permitida"
 			}); 	
		});
				

 	}else{
 		var userId = req.user.sub;

User.findOneAndUpdate({_id: userId}, {image: file_name}, {new: true}, (err, UserUpdated)=>{
 		if(err){
 			return res.status(404).send({
					status:'error',
					message:"Error al guardar el usuario"
					});
 		}
 		if(!UserUpdated){
 			return res.status(404).send({
					status:'error',
					message:"El usuario no se actualizo por falta de UserUpdated"
					});
 		}
 		return res.status(200).send({
 			status:'success',
 			user: UserUpdated
 		});
 	});

 	

 	}
 	//comprobar q el usuario q vamos a actualizar sea el identificado
 	//hacer el update(findOneAndUpdate)
 	//si no es extension valida Borrar el archivo subido
 	
 },
 avatar: function(req, res){
 	var  fileName = req.params.fileName;
 	var pathFile = './uploads/users/'+fileName;

 	fs.exists(pathFile, (exists)=>{
 		if(exists){
 			return res.sendFile(path.resolve(pathFile);
 		}else{
 			return res.status(404).send({
 				message:'la imagen no existe'			});
 		}
 	})
 },
 getUsers: function(req, res){
 	User.find().exec((err, users)=>{
 		if(err||!users){
 			return res.status(404).send({
 				status:'error',
 				message: 'No hay usuarios que mostrar'
 			});
 		}
 		return res.status(200).send({
 			status: 'success',
 			users
 		});
 	});
 },
 getUser: function (req, res){
 	var userId = req.params.userId;
 	User.findById(userId).exec((err, user)=>{
 		if(err||!users){
 			return res.status(404).send({
 				status:'error',
 				message: 'El usuario no existe'
 			});
 		}return res.status(200).send({
 			status: 'success',
 			user
 		});
 	});


 }

};


 module.exports = controller;